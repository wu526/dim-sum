#ifndef __KAPI_DIM_SUM_TASK_H
#define __KAPI_DIM_SUM_TASK_H

#define SCHED_FIFO		0
#define SCHED_TIMESLICE	1
#define SCHED_NORMAL	2
#define SCHED_SERVICE	3

#define TaskId unsigned long
typedef  int (*TASK_FUNCTPTR)(void *data) ;
extern TaskId create_process(int (*TaskFn)(void *data),
			void *pData,
			char *strName,
			int prio,
			int sched_policy
		);
#endif /* __KAPI_DIM_SUM_TASK_H */
